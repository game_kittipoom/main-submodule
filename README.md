# Main-Submodule

## Clone project
- `git clone git@bitbucket.org:game_kittipoom/main-submodule.git`

## Add submodule
- `git submodule add -b master git@bitbucket.org:game_kittipoom/submodule.git`

## Clone project with latest version submodule
- `git clone --recurse-submodules git@bitbucket.org:game_kittipoom/main-submodule.git`

## Change branch submodule
- `git config -f .gitmodules submodule.sub-project.branch develop`

## How to push submodule
- push only submodule
    - `cd submodule`
    - `git push (after add and commit)`
- push with main-submodule
    - `git push --recurse-submodules=on-demand`

## Remove Submodules
- `git submodule deinit -f submodule`
- `rm -rf .git/modules/submodule`
- `rm .gitmodules`
- `git commit -m "removed submodule" & git push`